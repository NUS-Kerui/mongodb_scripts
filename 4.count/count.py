import pymongo
import os
import re
from bson.int64 import Int64
import argparse
parser = argparse.ArgumentParser()
parser.add_argument( '--ip', type=str,
                    default="10.217.128.47")
parser.add_argument( '--port', type=int,
                    default=27017)
parser.add_argument('--database_name',type=str,
                    default="pushkin")
parser.add_argument('--target_table',type=str,
                    default="count_number")
args = parser.parse_args()

connection = pymongo.MongoClient(args.ip,args.port)
tdb = connection[args.database_name]

#read documents from DB.Tweets
temp_filtered = tdb['temp_filtered_text']
tweets_temp = tdb['tweets_temp']
comments_excluded = tdb['comments_excluded']
facebook = tdb['facebook']
youtube = tdb['youtube']
target_table = tdb[args.target_table]
tweets_relevant_count = temp_filtered.count({'corpus_type':'tweets_temp', 'text_type':'text'})
tweets_others_count = tweets_temp.count({'saf_related':-1})


youtube_relevant_count = youtube.count({'saf_related':1})
youtube_others_count = youtube.count({'saf_related': -1})

facebook_relevant_count = temp_filtered.count({'corpus_type':'facebook', 'text_type':'text'})
facebook_others_count = comments_excluded.count({'corpus_type':'facebook'})

forum_relevant_count = temp_filtered.count({'corpus_type':'forum', 'text_type':'text'})
forum_others_count = None

news_relevant_count = facebook.count({'formal_source':'news', 'saf_related':1})
news_others_count = facebook.count({'formal_source':'news', 'saf_related':-1})

blog_relevant_count = facebook.count({'formal_source':'blog', 'saf_related':1})
blog_others_count = facebook.count({'formal_source':'blog', 'saf_related':-1})
news_relevant_comments = temp_filtered.count({'corpus_type':'facebook','formal_source':'news','text_type':'text'})
blog_relevant_comments = temp_filtered.count({'corpus_type':'facebook','formal_source':'blog','text_type':'text'})
records = []
records.append({'_id':'tweets_relevant','count':format(tweets_relevant_count,',')})
records.append({'_id':'tweets_others','count':format(tweets_others_count,',')})
records.append({'_id':'youtube_relevant','count':format(youtube_relevant_count,',')})
records.append({'_id':'youtube_others','count':format(youtube_others_count,',')})
records.append({'_id':'facebook_relevant','count':format(facebook_relevant_count,',')})
records.append({'_id':'facebook_others','count':format(facebook_others_count,',')})
records.append({'_id':'forum_relevant','count':format(forum_relevant_count,',')})
records.append({'_id':'forum_others','count':forum_others_count})
records.append({'_id':'news_relevant','count':format(news_relevant_count,',')})
records.append({'_id':'news_others','count':format(news_others_count,',')})
records.append({'_id':'blog_relevant','count':format(blog_relevant_count,',')})
records.append({'_id':'blog_others','count':format(blog_others_count,',')})
records.append({'_id':'news_relevant_comments','count':format(news_relevant_comments,',')})
records.append({'_id':'blog_relevant_comments','count':format(blog_relevant_comments,',')})
for record in records:
	try:
		target_table.insert(record)
	except:
		pass
