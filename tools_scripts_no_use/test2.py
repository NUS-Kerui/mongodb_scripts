import pymongo
import json
corpus_type = 'youtube'
connection = pymongo.MongoClient("10.217.128.47",27017)
tdb = connection["pushkin"]

target_table = tdb['temp_filtered_text']
target_words = []
result0 = open('result_'+corpus_type+'_polarity0'+'.txt','a+')
result1 = open('result_'+corpus_type+'_polarity1'+'.txt','a+')
result2 = open('result_'+corpus_type+'_polarity2'+'.txt','a+')



tweets_0 = list(target_table.find({"corpus_type":corpus_type,'text_type':'text','polarity':0},{'text':1}))
tweets_1= list(target_table.find({"corpus_type":corpus_type,'text_type':'text','polarity':1},{'text':1}))
tweets_2 = list(target_table.find({"corpus_type":corpus_type,'text_type':'text','polarity':2},{'text':1}))
tweets_0 = '\n\n'.join([a['text'].strip().replace('\n',".") for a in tweets_0])
tweets_1 = '\n\n'.join([a['text'].strip().replace('\n',".") for a in tweets_1])
tweets_2 = '\n\n'.join([a['text'].strip().replace('\n',".") for a in tweets_2])

result0.write(tweets_0)
result1.write(tweets_1)
result2.write(tweets_2)

result0.close()
result1.close()
result2.close()