import os
import pymongo
import json
import datetime
# connect database and init
connection = pymongo.MongoClient('10.217.128.47',27017)
tdb = connection.pushkin

#read documents from DB.Tweets
table = tdb.topic_list_backup
table2 = tdb.temp_filtered_text
count = 0
docs = list(table.find({}))
for doc in docs:
	doc_id = doc['corpus_id']
	try:
		doc_with = list(table2.find({"corpus_id":doc_id},{'timestamp_ms':1,'timestamp_daily':1,'timestamp_monthly':1, 'polarity':1}))[0]
	except:
		count += 1
		continue
	query = {'corpus_id':doc_id}
	table.update(query,{'$set':{'timestamp_ms':doc_with['timestamp_ms']}})
	table.update(query,{'$set':{'timestamp_daily':doc_with['timestamp_daily']}})
	table.update(query,{'$set':{'timestamp_monthly':doc_with['timestamp_monthly']}})
	table.update(query,{'$set':{'polarity':doc_with['polarity']}})
print (count)



