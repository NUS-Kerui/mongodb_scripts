import pymongo
import json
connection = pymongo.MongoClient("10.217.128.47",27017)
tdb = connection["pushkin"]

target_table = tdb['target_list']

tweets = list(target_table.aggregate([
{'$match':{"corpus_type":"tweets_temp"}},
{'$group':{'_id':"$target_words",'sum': { '$sum': 1}}},
{'$sort':{"sum":-1}}
]))
with open("./tweets.txt","w") as f:
	for item in tweets:
		f.write(str(item['_id'])+':'+str(item['sum'])+'\n\n')

youtube = list(target_table.aggregate([
{'$match':{"corpus_type":"youtube"}},
{'$group':{'_id':"$target_words",'sum': { '$sum': 1}}},
{'$sort':{"sum":-1}}
]))
with open("./youtube.txt","w") as f:
	for item in youtube:
		f.write(str(item['_id'])+':'+str(item['sum'])+'\n\n')

facebook = list(target_table.aggregate([
{'$match':{"corpus_type":"facebook"}},
{'$group':{'_id':"$target_words",'sum': { '$sum': 1}}},
{'$sort':{"sum":-1}}
]))
with open("./facebook.txt","w") as f:
	for item in facebook:
		f.write(str(item['_id'])+':'+str(item['sum'])+'\n\n')

forum = list(target_table.aggregate([
{'$match':{"corpus_type":"forum"}},
{'$group':{'_id':"$target_words",'sum': { '$sum': 1}}},
{'$sort':{"sum":-1}}
]))
with open("./forum.txt","w") as f:
	for item in forum:
		f.write(str(item['_id'])+':'+str(item['sum'])+'\n\n')