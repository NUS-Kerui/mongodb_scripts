import pymongo
import os
import operator
from bson.int64 import Int64
import arrow
import time
import argparse
parser = argparse.ArgumentParser()
parser.add_argument( '--ip', type=str,
                    default="10.217.128.47")
parser.add_argument( '--port', type=int,
                    default=27017)
parser.add_argument('--database_name',type=str,
                    default="pushkin")
args = parser.parse_args()
# convert time to timestamp
def time_convertor(time_string, db_type):
	if db_type == 'youtube':
		u_time = arrow.get(time_string)
		return Int64(u_time.timestamp)
	elif db_type == 'facebook':
		try:
			f_time = arrow.get(time_string,'MMMM D YYYY  H:mmA')

		except:
			f_time = arrow.get(time_string,'D MMMM YYYY  H:mm')
			
		return f_time.timestamp
	else:
		print ('type is wrong')

connection = pymongo.MongoClient(args.ip,args.port)
tdb = connection['args.database_name']


#read documents from DB.Tweets
table = tdb.facebook
f_docs = list(table.find({'saf_related':1}))
f_docs += list(table.find({'saf_related':-1}))[:200]
table = tdb.youtube
y_docs = list(table.find({'saf_related':1}))
y_docs += list(table.find({'saf_related':-1}))[:200]
table = tdb.forum
b_docs = table.find({})

table = tdb.comments
for i, doc in enumerate(f_docs):
	simple_comments = []
	if 'comments' in doc:
		for a in doc['comments']:
			flag = 0
			for b in simple_comments:
				if a['id'] == b['id']:
					flag = 1
					continue
			if flag == 0:
				simple_comments.append(a)


	for j in range(len(simple_comments)):	
		new_doc  = {}
		try:
			print (simple_comments[j]['id'])
			if 'title' in doc:
				new_doc['_id'] = Int64(simple_comments[j]['id'])
				new_doc['news_id'] = doc['_id']
				new_doc['news_text'] = doc['title']
				new_doc['corpus_type'] = 'facebook'
				new_doc['text'] = simple_comments[j]['content']
				new_doc['author'] = simple_comments[j]['author']
				string_time = simple_comments[j]['date_time'].replace(',','')
				string_time = string_time.replace('at','').strip()
				string_time = ' '.join(string_time.split(' ')[1:])
				print (string_time)
				new_doc['date_time'] = time_convertor(string_time,'facebook')
				new_doc['formal_source'] = doc['formal_source']
				new_doc['saf_related'] = doc['saf_related']
				table.insert(new_doc)

		
			elif ('news_detail' in doc) and ('title' in doc['news_detail']):
				new_doc['_id'] = Int64(simple_comments[j]['id'])
				new_doc['news_id'] = doc['_id']
				new_doc['news_text'] = doc['news_detail']['title']
				new_doc['corpus_type'] = 'facebook'
				new_doc['text'] = simple_comments[j]['content']
				new_doc['author'] = simple_comments[j]['author']
				string_time = simple_comments[j]['date_time'].replace(',','')
				string_time = string_time.replace('at','').strip()
				string_time = ' '.join(string_time.split(' ')[1:])
				print (string_time)
				new_doc['date_time'] = time_convertor(string_time,'facebook')
				new_doc['formal_source'] = doc['formal_source']
				new_doc['saf_related'] = doc['saf_related']
				table.insert(new_doc)
			else:
				new_doc['_id'] = Int64(simple_comments[j]['id'])
				new_doc['news_id'] = doc['_id']
				new_doc['news_text'] = doc['content']
				new_doc['corpus_type'] = 'facebook'
				new_doc['text'] = None
				new_doc['author'] = simple_comments[j]['author']
				string_time = simple_comments[j]['date_time'].replace(',','')
				string_time = string_time.replace('at','').strip()
				string_time = ' '.join(string_time.split(' ')[1:])
				print (string_time)
				new_doc['date_time'] = time_convertor(string_time,'facebook')
				new_doc['formal_source'] = doc['formal_source']
				new_doc['saf_related'] = doc['saf_related']
				table.insert(new_doc)
		except:
			if 'title' in doc:

				new_doc['_id'] = doc['_id']+'_'+simple_comments[j]['id']
				
				new_doc['news_id'] = doc['_id']
				new_doc['news_text'] = doc['title']
				new_doc['corpus_type'] = 'facebook'
				new_doc['text'] = simple_comments[j]['content']
				new_doc['author'] = simple_comments[j]['author']
				string_time = simple_comments[j]['date_time'].replace(',','')
				string_time = string_time.replace('at','').strip()
				string_time = ' '.join(string_time.split(' ')[1:])
				print (string_time)
				new_doc['date_time'] = time_convertor(string_time,'facebook')
				new_doc['formal_source'] = doc['formal_source']
				new_doc['saf_related'] = doc['saf_related']
				table.insert(new_doc)
			elif ('news_detail' in doc) and ('title' in doc['news_detail']):
				new_doc['_id'] = doc['_id']+'_'+simple_comments[j]['id']
				new_doc['news_id'] = doc['_id']
				new_doc['news_text'] = doc['news_detail']['title']
				new_doc['corpus_type'] = 'facebook'
				new_doc['text'] = simple_comments[j]['content']
				new_doc['author'] = simple_comments[j]['author']
				string_time = simple_comments[j]['date_time'].replace(',','')
				string_time = string_time.replace('at','').strip()
				string_time = ' '.join(string_time.split(' ')[1:])
				print (string_time)
				new_doc['date_time'] = time_convertor(string_time,'facebook')
				new_doc['formal_source'] = doc['formal_source']
				new_doc['saf_related'] = doc['saf_related']
				table.insert(new_doc)

			else:
				new_doc['_id'] = doc['_id']+'_'+simple_comments[j]['id']
				new_doc['news_id'] = doc['_id']
				new_doc['news_text'] = doc['content']
				new_doc['corpus_type'] = 'facebook'
				new_doc['text'] = None
				new_doc['author'] = simple_comments[j]['author']
				string_time = simple_comments[j]['date_time'].replace(',','')
				string_time = string_time.replace('at','').strip()
				string_time = ' '.join(string_time.split(' ')[1:])
				print (string_time)
				new_doc['date_time'] = time_convertor(string_time,'facebook')
				new_doc['formal_source'] = doc['formal_source']
				new_doc['saf_related'] = doc['saf_related']
				table.insert(new_doc)

for i, doc in enumerate(y_docs):
	if 'comments' in doc:
		for j in range(len(doc['comments'])):
			new_doc  = {}
			new_doc['_id'] = doc['comments'][j]['_id']
			new_doc['news_id'] = doc['_id']
			new_doc['news_text'] = doc['title']
			new_doc['corpus_type'] = 'youtube'
			new_doc['text'] = doc['comments'][j]['content']
			new_doc['author'] = doc['comments'][j]['author']
			new_doc['date_time'] = time_convertor(doc['comments'][j]['date_time'],'youtube')
			new_doc['author_url'] = doc['comments'][j]['author_url']
			new_doc['like_count'] = doc['comments'][j]['like_count']
			new_doc['dislike_count'] = doc['comments'][j]['dislike_count']
			new_doc['formal_source'] = doc['formal_source']
			new_doc['saf_related'] = doc['saf_related']
			table.insert(new_doc)


for i, doc in enumerate(b_docs):
	simple_comments = []
	if 'posts' in doc:
		for a in doc['posts']:
			flag = 0
			for b in simple_comments:
				try:
					if a['id'] == b['id']:
						flag = 1
						continue
				except:
					break
			if flag == 0:
				simple_comments.append(a)
	if 'posts' in doc:
		for j in range(len(simple_comments)):
			new_doc  = {}
			new_doc['news_id'] = doc['_id']
			new_doc['news_text'] = doc['title']
			new_doc['corpus_type'] = 'forum'
			new_doc['followers'] = 0
			for item in simple_comments[j]:
				if item != 'posts':
					if item == 'id':
						new_doc['_id'] = simple_comments[j][item]
					else:
						new_doc[item] = simple_comments[j][item]
				else:
					for l in range(len(simple_comments[j]['posts'])):
						for item in simple_comments[j]['posts'][l]:
							if item != 'posts':
								if item == 'id':
									new_doc['_id'] = simple_comments[j]['posts'][l][item]
								else:
									new_doc[item] = simple_comments[j]['posts'][l][item]
			table.insert(new_doc)


