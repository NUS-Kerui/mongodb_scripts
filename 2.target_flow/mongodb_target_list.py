import argparse
import os
import re
import string

import pymongo

parser = argparse.ArgumentParser()
parser.add_argument("--type", type=str,
                    help="tweets_temp, facebook ,forum, youtube",
                    default="tweets_temp")
parser.add_argument("--target_words_dir", type=str,
                    default="./target_words")
parser.add_argument("--ip", type=str,
                    default="10.217.128.47")
parser.add_argument("--port", type=int,
                    default=27017)
parser.add_argument("--source_table", type=str,
                    default="temp_filtered_text")
parser.add_argument("--target_table", type=str,
                    default="target_list")
parser.add_argument("--database_name", type=str,
                    default="pushkin")
parser.add_argument("-l", "--limit", type=int, default=300000)
parser.add_argument("-sk", "--skip", type=int, default=0)

args = parser.parse_args()
# topics = ["leader"s square , pasir laba camp", "national service resort country club",
#           "national service training institute", "pasir laba camp", "nee soon camp", "singapore infantry regiment",
#           "home team ns", "pulau tekong", "field camp", "training institute", "ns life", "service training",
#           "safra resort", "selarang camp", "kranji camp", "army life", "maju camp", "army boys", "bedok camp",
#           "platoon mates", "platton mate", "enlistment", "pasir", "kranji", "tekong", "stagmont", "commandos",
#           "tampines", "jurong", "lim chu kang", "ocs", "mandai", "sarimbun", "mindef", "bookout", "navy", "cadets",
#           "air force", "uniform", "nsmen", "ord", "hendon", "selarang", "khatib", "safti", "nee soon", "pasir ris",
#           "scdf", "cadet", "ippt", "rsaf", "platoon", "paya lebar", "mandai hill", "pasir laba", "bmt", "ncc",
#           "infantry", "saf", "camp mate", "keat hong", "reservist", "sembawang", "amoy quee", "bedok", "military",
#           "sungei gedong", "seletar", "clementi", "national service", "commando", "maju", "hq scdf 3rd division",
#           "arc tampines", "mosquito commando", "reservist trainers", "officer cadet school", "ocs buddy"]
target = []
if args.type == "tweets_temp":
    filename = os.path.join(args.target_words_dir, "tweets.txt")
    print("Open file: {}".format(filename))
    with open(filename, "r") as f:
        for line in f.readlines():
            if line != "\n":
                word = line.split(":")[0].strip()
                if word not in target:
                    target.append(word)
elif args.type == "youtube":
    filename = os.path.join(args.target_words_dir, "youtube.txt")
    print("Open file: {}".format(filename))
    with open(filename, "r") as f:
        for line in f.readlines():
            if line != "\n":
                word = line.split(":")[0].strip()
                if word not in target:
                    target.append(word)
elif args.type == "facebook":
    filename = os.path.join(args.target_words_dir, "facebook.txt")
    print("Open file: {}".format(filename))
    with open(filename, "r") as f:
        for line in f.readlines():
            if line != "\n":
                word = line.strip()
                if word not in target:
                    target.append(word)
        print (len(target))
elif args.type == "forum":
    filename = os.path.join(args.target_words_dir, "forum.txt")
    print("Open file: {}".format(filename))
    with open(filename, "r") as f:
        for line in f.readlines():
            if line != "\n":
                word = line.strip()
                if word not in target:
                    target.append(word)

connection = pymongo.MongoClient(args.ip, args.port)
tdb = connection[args.database_name]

# read documents from DB.Tweets
table = tdb[args.source_table]
docs = table.find({"text_type": "normalized", "corpus_type": args.type}).limit(args.limit).skip(args.skip)
count = 0
target_table = tdb[args.target_table]
target_bulk = target_table.initialize_ordered_bulk_op()
corpus_type = args.type

if docs.count(with_limit_and_skip=True) > 0:
    for number, doc in enumerate(docs):
        txt_id = "-".join(doc["_id"].split("-")[0:-1]) + "-text"
        txt_doc = table.find_one({"_id": txt_id})
        org_txt = doc["text"]
        exclude = set(string.punctuation)
        org_txt = ''.join(ch for ch in org_txt if ch not in exclude)
        words_list = org_txt.split()
        for word in target:
            target_location = []
            indexs = [(m.start(0), m.end(0)) for m in re.finditer(r"\b({0})\b".format(word), org_txt)]
            if indexs != []:
                doc_id = str(doc["corpus_id"]) + "-" + corpus_type + "-0-" + word

                for number in indexs:
                    start_index, end_index = number[0], number[1]
                    matched_string = org_txt[start_index:end_index].split()
                    if len(matched_string) == 1:
                        matched_first_word = matched_string[0]
                        first_index = words_list.index(matched_first_word) + 1
                        target_location.append([first_index])
                    else:
                        matched_first_word = matched_string[0]
                        matched_end_word = matched_string[-1]
                        first_index = words_list.index(matched_first_word) + 1
                        end_index = words_list.index(matched_end_word) + 1

                        target_location.append([i for i in range(first_index, end_index + 1)])

                record = {
                    "text_normalized": org_txt,
                    "tokens": doc["tokens"],
                    "text": txt_doc["text"],
                    "corpus_type": corpus_type,
                    "corpus_id": doc["corpus_id"],
                    "target_order": 0,
                    "target_words": word,
                    "target_location": target_location,
                    "news_id": doc["news_id"],
                    "news_text": doc["news_text"],
                    "timestamp_ms": doc["timestamp_ms"],
                    "timestamp_daily": doc["timestamp_daily"],
                    "timestamp_monthly": doc["timestamp_monthly"]
                }
                target_bulk.find(selector={"_id": doc_id}). \
                    upsert(). \
                    update({"$set": record})
                count += 1
                if count % 100 == 0:
                    print("Upsert {} records to target_list".format(count))

    result = target_bulk.execute()
    print(result)

print("Done")

