import pymongo
import os
import re
from bson.int64 import Int64
import argparse
parser = argparse.ArgumentParser()
parser.add_argument( '--ip', type=str,
                    default="10.217.128.47")
parser.add_argument( '--port', type=int,
                    default=27017)
parser.add_argument('--database_name',type=str,
                    default="pushkin")
parser.add_argument('--source_table',type=str,
                    default="ner_list")
parser.add_argument('--target_table',type=str,
                    default="ner_keywords")
args = parser.parse_args()


connection = pymongo.MongoClient(args.ip,args.port)
tdb = connection[args.database_name]

#read documents from DB.Tweets
source_table = tdb[args.source_table]
target_table = tdb[args.target_table]
docs = list(source_table.find({},{'ner_type':1,'ner_words':1}))
for doc in docs:
	if doc['ner_type'] == 'LOC' or doc['ner_type'] == 'GPE':
		record = {'_id':'LOC/GPE_'+doc['ner_words'],
				  'ner_type':'LOC/GPE',
				  'word':doc['ner_words']

		}
	else:
		record =  {'_id':doc['ner_type']+'_'+doc['ner_words'],
				  'ner_type':doc['ner_type'],
				  'word':doc['ner_words']
		}
	if target_table.count({'_id':record['_id']}) == 0:
		target_table.insert(record)
	else:
		continue