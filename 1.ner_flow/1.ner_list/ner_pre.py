import argparse
import os
import re

import pymongo

parser = argparse.ArgumentParser()
parser.add_argument("--type", type=str,
                    help="tweets_temp, facebook ,forum, youtube, facebook_content",
                    default="tweets_temp")
parser.add_argument("--ip", type=str,
                    default="10.217.128.47")
parser.add_argument("--port", type=int,
                    default=27017)
parser.add_argument("--input_dir", type=str,
                    default="/home/liuman/project/pushkin-main/workspace/ner-web-service/input_dir")
parser.add_argument("--output_dir", type=str,
                    default="/home/liuman/project/pushkin-main/workspace/ner-web-service/output_dir")
parser.add_argument("--source_table", type=str,
                    default="temp_filtered_text")
parser.add_argument("--database_name", type=str,
                    default="pushkin")
parser.add_argument("--process_tweet_path", type=str,
                    default="./")
parser.add_argument("-l", "--limit", nargs="?", type=int, metavar="<limit>", default=1000,
                    help="Number of records to be filtered (default: %(default)s).")
parser.add_argument("-sk", "--skip", nargs="?", type=int, metavar="<skip>", default=0,
                    help="Number of records to be skipped (default: %(default)s).")

args = parser.parse_args()
input_dir = args.input_dir
output_dir = args.output_dir

PROCESS_DICT = dict(
    tweets=dict(process="process-tweet.sh"),
    tweets_temp=dict(process="process-tweet.sh"),
    youtube=dict(process="process-youtube.sh"),
    facebook=dict(process="process-youtube.sh"),
    forum=dict(process="process-forum.sh"),
    facebook_content=dict(process="process-formal.sh")
)


def remove_punc(txt):
    punc = "[,.?!\"@]"
    return re.sub(punc, " ", txt)


# connect database and init
connection = pymongo.MongoClient(args.ip, args.port)
tdb = connection[args.database_name]

# read documents from DB.Tweets
name_txt = "ids.txt"
path1 = os.path.join(input_dir, "input.txt")
path2 = os.path.join(output_dir, name_txt)
if os.path.exists(path1):
    os.remove(path1)
if os.path.exists(path2):
    os.remove(path2)

f1 = open(path1, "a")
f2 = open(path2, "a")

if args.type == "facebook_content":
    table = tdb["facebook"]
    docs = list(table.find({"saf_related": 1}).limit(args.limit).skip(args.skip))
    for doc in docs:
        try:
            # if count < 1000:
            name = doc["_id"]
            text = doc["title"].replace("\r", "") + ". " + doc["content"].replace("\r", "")
            text = text.replace("\n", "").lower()
            text = remove_punc(text)
            f1.write(text)
            f1.write("\n\n")
            f2.write(str(name))
            f2.write("\t")

        #	count += 1
        except Exception as e:
            print(e)
else:
    table = tdb[args.source_table]
    print("args.source_table:", args.source_table)
    docs = list(table.find({"text_type": "normalized", "corpus_type": args.type, "text": {"$ne": ""}}).limit(args.limit).skip(args.skip))
    print("text_type: normalized, corpus_type:", args.type, ", limit:", args.limit, ", skip:", args.skip)
    for doc in docs:
        try:
            # if count < 1000:
            name = doc["corpus_id"]
            
            text = doc["text"].replace("\r", "")
            text = text.replace("\n", "")
            f1.write(text)
            f1.write("\n\n")
            f2.write(str(name))
            f2.write("\t")

        #	count += 1
        except Exception as e:
            print (doc["corpus_id"])

f1.close()
f2.close()

# run model

input_txt_path = os.path.join(input_dir, "input.txt")
output_txt_path = os.path.join(output_dir, "output.txt")
f = open(output_txt_path, "w")
f.close()

command = os.path.join(args.process_tweet_path, PROCESS_DICT[args.type]["process"]) \
          + " " + input_txt_path + " " + output_txt_path
os.system(command)

