import pymongo
import os
import re
import argparse
from bson.int64 import Int64
import copy
parser = argparse.ArgumentParser()
parser.add_argument( '--type', type=str,
                    help='tweets_temp, facebook ,forum, youtube, facebook_content',
                    default="tweets_temp")
parser.add_argument( '--ip', type=str,
                    default="10.217.128.47")
parser.add_argument( '--port', type=int,
                    default=27017)
parser.add_argument( '--target_table', type=str,
                    default="ner_list")
parser.add_argument( '--source_table', type=str,
                    default="temp_filtered_text")
parser.add_argument('--model_output', type=str,
                    default="./output_dir")
parser.add_argument('--database_name',type=str,
                    default="pushkin")
args = parser.parse_args()
def combine_words(words,class_names):
    class_title =  copy.deepcopy(class_names)
    for i,name in enumerate(class_names):
        if name != 'O':
            class_title[i] = name.split('-')[0]
            class_names[i] = name.split('-')[1]
    #class_names = [name.split('-')[1] for name in class_names if name != 'O'else 'O']
    combined_words = []
    combined_class = []
    indexs = []
    head = 0
    tail = 1
    while tail < len(words):
        if class_title[head] == 'B' and class_title[tail] == 'I' and class_names[head] == class_names[tail]:
            tail += 1
        else:
            combined_words.append(' '.join(words[head:tail]))
            indexs.append([i for i in range(head+1,tail+1)])
            combined_class.append(class_names[head])
            head = tail
            tail += 1
    return combined_words, combined_class, indexs

def get_words_in_class(list1):
        words = []
        for dic in list1:
                words.append(list(dic.keys())[0])
        return words
# def remove_punc(txt):
#     punc = '[,.!\'@]'
#     return re.sub(punc, '', txt)
output_dir = args.model_output
print("output_dir:", output_dir)
id_doc = os.path.join(output_dir, 'ids.txt')
f = open(id_doc,'r')
ids = f.readline()
ids = ids.split('\t')[:-1]
connection = pymongo.MongoClient(args.ip,args.port)
tdb = connection[args.database_name]

#read documents from DB.Tweets
target_table = tdb[args.target_table]
#table2 = tdb.tweets_temp
corpus_type = args.type
output_txt_path = os.path.join(output_dir,'output.txt')
with open(output_txt_path, 'r') as f:
        docs = f.read()
        docs = docs.split('\n\n')[:-1]
        print(len(docs))
        for i, doc in enumerate(docs):
                text = ""
                class_names= []
                words = []
                doc_id = ids[i]
                if args.type != 'facebook_content':
                    table = tdb[args.source_table]
                    normalized_doc_id = doc_id+'-'+corpus_type+'-normalized'
                    print ("normalized_doc_id:", normalized_doc_id)
                    text_doc_id = doc_id+'-'+corpus_type+'-text'
                    print("text_doc_id:", text_doc_id)
                    normalized_doc = list(table.find({'_id':normalized_doc_id}))[0]
                    org_doc = list(table.find({'_id':text_doc_id}))[0]
                    try:
                        doc_id = Int64(doc_id)
                    except:
                        pass
                    #tweets_temp_doc = list(table2.find({'_id':Int64(doc_id)}))[0]

                    for line in doc.split('\n'):
                            if line != '':
                                    [word, class_name] = line.split('\t')
                                    text += word+' '
                                    class_names.append(class_name)
                                    words.append(word)
                    words, class_names, indexs = combine_words(words, class_names)
                    # table.insert(record)
                    for j, word in enumerate(words):
                            if class_names[j] != 'O':
                                    record = {
                                        "_id": "",
                                        "corpus_type":corpus_type,
                                        "corpus_id": doc_id,
                                        "ner_type": "",
                                        "ner_order": 0,
					"news_id":normalized_doc['news_id'],
                                        "text_normalized": normalized_doc['text'],
                                        "tokens": normalized_doc['tokens'],
                                        "text": org_doc['text'],
                                        "ner_words": word,
                                        "ner_location":[indexs[j]],
                                        "timestamp_ms": org_doc['timestamp_ms'],
                                        "timestamp_daily": org_doc['timestamp_daily'],
                                        "timestamp_monthly":org_doc['timestamp_monthly']
                                        }
                                    if class_names[j] == 'GPE':
                                        record["_id"] = str(doc_id)+'-'+corpus_type+'-'+'GPE'+'-0-'+word
                                        record["ner_type"] = "GPE"
                                    elif class_names[j] == 'PER':
                                        record["_id"] = str(doc_id)+'-'+corpus_type+'-'+'PER'+'-0-'+word
                                        record["ner_type"] = "PER"

                                    elif class_names[j] == 'DTM':
                                        record["_id"] = str(doc_id)+'-'+corpus_type+'-'+'DTM'+'-0-'+word
                                        record["ner_type"] = "DTM"

                                    elif class_names[j] == 'LOC':
                                        record["_id"] = str(doc_id)+'-'+corpus_type+'-'+'LOC'+'-0-'+word
                                        record["ner_type"] = "LOC"

                                    elif class_names[j] == 'FAC':
                                        record["_id"] = str(doc_id)+'-'+corpus_type+'-'+'FAC'+'-0-'+word
                                        record["ner_type"] = "FAC"

                                    elif class_names[j] == 'ORG':
                                        record["_id"] = str(doc_id)+'-'+corpus_type+'-'+'ORG'+'-0-'+word
                                        record["ner_type"] = "ORG"
                                    doc_list = list(target_table.find({"_id":record['_id']}))
                                    if len(doc_list) == 0:
                                        target_table.insert(record)
                                    else:
                                        #print(record["_id"])
                                        new_indexs = doc_list[0]["ner_location"]
                                        new_indexs.append(indexs[j])
                                        target_table.update({"_id":record["_id"]},{'$set':{'ner_location':new_indexs}})
                else:
                    table = tdb['facebook']
                    org_doc = list(table.find({'_id':doc_id}))[0]

                    for line in doc.split('\n'):
                            if line != '':
                                    [word, class_name] = line.split('\t')
                                    text += word+' '
                                    class_names.append(class_name)
                                    words.append(word)
                    words, class_names, indexs = combine_words(words, class_names)
                    # table.insert(record)
                    for j, word in enumerate(words):
                            if class_names[j] != 'O':
                                    record = {
                                        "_id": "",
                                        "corpus_type":corpus_type,
                                        "corpus_id": doc_id,
                                        "ner_type": "",
                                        "ner_order": 0,
                                        "text": org_doc['content'].lower(),
					"title":org_doc['title'].lower(),
                                        "ner_words": word,
                                        "ner_location":[indexs[j]],
                                    }
                                    if class_names[j] == 'GPE':
                                        record["_id"] = str(doc_id)+'-'+corpus_type+'-'+'GPE'+'-0-'+word
                                        record["ner_type"] = "GPE"
                                    elif class_names[j] == 'PER':
                                        record["_id"] = str(doc_id)+'-'+corpus_type+'-'+'PER'+'-0-'+word
                                        record["ner_type"] = "PER"
                                    elif class_names[j] == 'DTM':
                                        record["_id"] = str(doc_id)+'-'+corpus_type+'-'+'DTM'+'-0-'+word
                                        record["ner_type"] = "DTM"
                                    elif class_names[j] == 'LOC':
                                        record["_id"] = str(doc_id)+'-'+corpus_type+'-'+'LOC'+'-0-'+word
                                        record["ner_type"] = "LOC"
                                    elif class_names[j] == 'FAC':
                                        record["_id"] = str(doc_id)+'-'+corpus_type+'-'+'FAC'+'-0-'+word
                                        record["ner_type"] = "FAC"
                                    elif class_names[j] == 'ORG':
                                        record["_id"] = str(doc_id)+'-'+corpus_type+'-'+'ORG'+'-0-'+word
                                        record["ner_type"] = "ORG"
                                    doc_list = list(target_table.find({"_id":record['_id']}))
                                    if len(doc_list) == 0:
                                        target_table.insert(record)
                                    else:
                                        #print(record["_id"])
                                        new_indexs = doc_list[0]["ner_location"]
                                        new_indexs.append(indexs[j])
                                        target_table.update({"_id":record["_id"]},{'$set':{'ner_location':new_indexs}})

