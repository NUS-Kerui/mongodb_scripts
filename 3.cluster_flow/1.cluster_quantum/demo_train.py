from argparse import ArgumentParser
import logging
import pymongo
from uriutils import URIFileType
import sys
from ycsettings import Settings
import numpy as np
from ..featurizers import load_featurized
from ..utils import get_class_from_module_path
np.set_printoptions(threshold=np.inf)
logger = logging.getLogger(__name__)


def main():
    parser = ArgumentParser(description='Classify instances using ML classifier.')
    parser.add_argument('-s', '--settings', dest='settings_uri', type=URIFileType(), metavar='<settings_file>', help='Settings file to configure models.')

    parser.add_argument('classifier_type', type=str, metavar='<classifier_type>', nargs='?', help='Type of classifier model to fit.')
    parser.add_argument('-o', '--output', type=URIFileType('wb'), metavar='<classifier_file>', help='Save trained classifier model here.')
    parser.add_argument('-r', '--resume', type=str, metavar='<param>', nargs='+', help='Resume training from given file (takes multiple arguments depending on classifier).')
    parser.add_argument('-v', '--validation-data', type=URIFileType(), metavar='<featurized>', help='Use this as validation set instead of system defined one.')
    parser.add_argument( '--type', type=str,
                    help='tweets_temp, youtube',
                    default="tweets_temp")
    parser.add_argument( '--ip', type=str,
                    default="10.217.128.47")
    parser.add_argument( '--port', type=int,
                    default=27017)
    parser.add_argument('--database_name',type=str,
                    default="pushkin")
    parser.add_argument( '--source_table', type=str,
                    default="topic_list")
    parser.add_argument( '--lookup_table', type=str,
                    default="temp_filtered_text")

    A = parser.parse_args()

    settings = Settings(A)
    log_level = settings.get('log_level', default='DEBUG').upper()
    log_format = settings.get('log_format', default='%(asctime)-15s [%(name)s-%(process)d] %(levelname)s: %(message)s')
    logging.basicConfig(format=log_format, level=logging.getLevelName(log_level))

    classifier_type = settings.get('classifier_type')
    classifier_parameters = settings.get('classifier_parameters', default={})
    classifier_parameters['n_jobs'] = settings.getnjobs('n_jobs', default=1)

    classifier_class = get_class_from_module_path(classifier_type)
    if not classifier_class: parser.error('Unknown classifier name "{}".'.format(classifier_type))

    kwargs = dict(fit_args={})
    if A.validation_data:
        X_validation, Y_validation = load_featurized(A.validation_data, keys=('X_featurized', 'Y_labels'))
        kwargs['validation_data'] = (X_validation, Y_validation)
    #end if

    if A.resume: kwargs['fit_args']['resume'] = A.resume
    X_featurized = []
    Y_labels = []
    doc_ids =[]
    connection = pymongo.MongoClient(A.ip,A.port)
    tdb = connection[A.database_name]

    #read documents from DB.Tweets
    table = tdb[A.source_table]
    lookup_table = tdb[A.lookup_table]
    if A.type == 'tweets_temp':
        topic_id = 4
        docs = list(table.find({'topic_id':topic_id,'corpus_type':A.type},{'corpus_id':1}).limit(1000000))
        for doc in docs:
            X_featurized.append(list(lookup_table.find({'corpus_id':doc['corpus_id'],'text_type':'text_featurized'}))[0]['text'])
            Y_labels.append(['others'])
            doc_ids.append(doc['corpus_id'])

    elif A.type == 'youtube':
        topic_id = 2
        docs = list(table.find({'topic_id':topic_id,'saf_related':1},{'_id':1,'text_featurized':1}))
        for doc in docs:
            X_featurized.append(doc['text_featurized'])
            Y_labels.append(['others'])
            doc_ids.append(doc['_id'])

    __console__ = sys.stdout
    f_handler=open('tmp.log', 'w')
    sys.stdout=f_handler
    classifier = classifier_class(**classifier_parameters).fit(np.array(X_featurized), np.array(Y_labels), **kwargs)
    if A.output: classifier.save(A.output)
#end def
    f_handler.close()
    sys.stdout = __console__
    look_table = tdb.lookup_cluster
    cluster_table = tdb.cluster_list
    f = open('tmp.log','r')
    info = f.read().split('\n\n')
    ids_txt = info[0].strip()[1:-1].replace('\n','')
    demo_show_dic = eval(info[1].strip())
    doc_cluster_ids = []
    doc_cluster_ids = [int(i) for i in ids_txt.split()]
    cluster_ids = list(set(doc_cluster_ids))
    for i in cluster_ids:
        if demo_show_dic[i] == True:
            show_demo = 0
        else:
            show_demo = -1
        look_doc ={
                                "_id":str(i)+'-'+A.type,
                                "cluster_id": i,
                                "cluster_name": "Cluster"+str(i),
                                "flag":0,
                                "corpus_type":A.type,
                                "show_demo": show_demo
        }
        try:
            look_table.insert(look_doc)
        except:
            print (i)
    for i,d in enumerate(doc_ids):
        if A.type == 'tweets_temp':
            searched_doc = list(table.find({'corpus_id':d}))[0]
        elif A.type == 'youtube':
            searched_doc = list(table.find({'_id':d}))[0]

        try:
            if A.type == 'tweets_temp':               
                cluster_doc = {'corpus_id':d,
                                    '_id':str(d)+'-'+A.type,
                                    "text":searched_doc['text'],
                                    'cluster_id':str(doc_cluster_ids[i])+'-'+A.type,
                                    "corpus_type": A.type,
                                    "polarity": searched_doc['polarity'],
                                    "text_type":searched_doc['text_type'],
                                    "timestamp_ms": searched_doc['timestamp_ms'],
                                    "timestamp_daily": searched_doc['timestamp_daily'],
                                    "timestamp_monthly": searched_doc['timestamp_monthly']


                                    }
            elif A.type == "youtube":
                cluster_doc = {'corpus_id':d,
                                    '_id':str(d)+'-'+A.type,
                                    "text":searched_doc['title'],
                                    'cluster_id':str(doc_cluster_ids[i])+'-'+A.type,
                                    "corpus_type": A.type,
                                    "text_type": 'text_featurized',
                                    "timestamp_ms": searched_doc['timestamp_ms'],
                                    "timestamp_daily": searched_doc['timestamp_daily'],
                                    "timestamp_monthly": searched_doc['timestamp_monthly']




                                    }

            

            cluster_table.insert(cluster_doc)

        except Exception as e:
            pass

if __name__ == '__main__': main()
