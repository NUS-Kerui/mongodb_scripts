python -m ycml.scripts.demo_train \
       --settings settings/youtube/bow_clusterer_unsupervised.settings.yaml \
       -o models/youtube.bow_unsupervised_clusterer.gz \
       --type youtube \
       --source_table youtube
       #--type youtube
