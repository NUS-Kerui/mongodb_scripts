import pymongo 
import re,collections
from nltk.corpus import stopwords
connection = pymongo.MongoClient("10.217.128.47",27017)
tdb = connection["pushkin"]

#read documents from DB.Tweets
cluster_name_table = tdb['lookup_cluster']
cluster_table = tdb['cluster_list']
cluster_name = list(cluster_name_table.find({},{'_id':1}))
for c in cluster_name:
	c_id = c['_id']
	text = ''
	cluster_docs = cluster_table.find({'cluster_id':c_id})
	for c_d in cluster_docs:
		text =text+' '+c_d['text']
	stop_words = stopwords.words('english')
	stop_words.extend(["__url__","__mention__", ".", ",", "/", "\\", "!",
                   "?", "@", "!\\", ":", "...", "(", "'", "-",")", ";",
                   "&", "..", "~", ".\\", "\"", "-&","#","###","##","i'm","!!","(@","))"])
	
	# emoji_pattern = re.compile(
	# 	 u"(\ud83d[\ude00-\ude4f])|" # emoticons 
	# 	 u"(\ud83c[\udf00-\uffff])|" # symbols & pictographs (1 of 2) 
	# 	 u"(\ud83d[\u0000-\uddff])|" # symbols & pictographs (2 of 2) 
	# 	 u"(\ud83d[\ude80-\udeff])|" # transport & map symbols 
	# 	 u"(\ud83c[\udde0-\uddff])" # flags (iOS) "+", flags=re.UNICODE)
	# 	 )


	emoji_pattern = re.compile("["
                u"\U0001F600-\U0001F64F"  # emoticons
                u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                u"\U0001F680-\U0001F6FF"  # transport & map symbols
                u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                u"\U00002702-\U000027B0"
                u"\U000024C2-\U0001F251"
                u"\U0001f926-\U0001f937"
                u'\U00010000-\U0010ffff'
                u"\u200d"
                u"\u2640-\u2642"
                u"\u2600-\u2B55"
                u"\u23cf"
                u"\u23e9"
                u"\u231a"
                u"\u3030"
                u"\ufe0f"
    "]+", flags=re.UNICODE)


	text = emoji_pattern.sub(r'', text)
	words_box = []
	for word in text.split():
		if word not in (stop_words):
			words_box.append(word)
	top10_tuple = collections.Counter(words_box).most_common(10)
	top10_list = [word[0] for word in top10_tuple]
	try:
		cluster_name_table.update({'_id':c_id},{'$set':{'keywords':top10_list}})
	except:
		print (c_id)


