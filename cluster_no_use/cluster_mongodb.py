import logging
import argparse
import pymongo
from bson.int64 import Int64
logging.basicConfig(level=logging.DEBUG,
                    filename='output.log',
                    datefmt='%Y/%m/%d %H:%M:%S',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(lineno)d - %(module)s - %(message)s')
logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser()
parser.add_argument( '--ip', type=str,
                    default="10.217.128.47")
parser.add_argument( '--port', type=int,
                    default=27017)
parser.add_argument('--database_name',type=str,
                    default="pushkin")
parser.add_argument( '--type', type=str,
                    help='tweets_temp, facebook ,forum, youtube',
                    default="tweets_temp")
parser.add_argument( '--summary_path',type=str,
                    help='the path of cluster_summary',
                    default = "./tweets_summary.txt")
parser.add_argument( '--source_table',type=str,
                    default="topic_list")
args = parser.parse_args()
# connect database and init
connection = pymongo.MongoClient(args.ip,args.port)
tdb = connection[args.database_name]

#read documents from DB.Tweets
look_table = tdb.lookup_cluster
cluster_table = tdb.cluster_list
f = open(args.summary_path,'r')
txt = f.read()
clusters = txt.split('\n\n')[:-1]
search_table = tdb[args.source_table]
for i,cluster in enumerate(clusters):
        look_doc ={
                                "_id":str(i)+'-'+args.type,
                                "cluster_id": i,
                                "cluster_name": "Clutser"+str(i),
                                "flag":0,
                                "corpus_type":args.type
        }
        try:
            look_table.insert(look_doc)
        except:
            pass
        lines = cluster.split('\n')[1:]
        for line in lines:
                doc_id = line.split('\t')[-1].strip()
                try:
                        doc_id = Int64(doc_id)
                except:
                    pass
                
                searched_doc = list(search_table.find({'corpus_id':doc_id}))[0]
                try:
                        
                    cluster_doc = {'corpus_id':doc_id,
                                    '_id':str(doc_id)+'-'+args.type,
                                    "text":searched_doc['text'],
                                    'cluster_id':str(i)+'-'+args.type,
                                    "corpus_type": args.type,
                                    "polarity": searched_doc['polarity'],
                                    "text_type":'text',
                                    "timestamp_ms": searched_doc['timestamp_ms'],
                                    "timestamp_daily": searched_doc['timestamp_daily'],
                                    "timestamp_monthly": searched_doc['timestamp_monthly']




                                    }

                    cluster_table.insert(cluster_doc)

                except:
                        logger.info(str(doc_id)+' has errors!')
                                                                          
