import os
import pymongo
import json
import datetime
import argparse
import time
parser = argparse.ArgumentParser(description='')
parser.add_argument( '--type', type=str,
                    help='tweets_temp, facebook ,forum, youtube',
                    default="tweets_temp")
parser.add_argument( '--ip', type=str,
                    default="10.217.128.47")
parser.add_argument( '--port', type=int,
                    default=27017)
parser.add_argument('--database_name',type=str,
                    default="pushkin")
parser.add_argument( '--source_table', type=str,
                    default="topic_list")
parser.add_argument( '--lookup_table', type=str,
                    help='tweets_temp or comments',
                    default="tweets_temp")
parser.add_argument('--tmp_json',type=str,
                    default="./data/tweets/tweets_without_cluster.json")
parser.add_argument('--model_config',type=str,
                    default="./settings/tweets/incremental_clusterer.settings.yaml")
parser.add_argument('--model_output',type=str,
                    default="./tweets_summary.txt")
parser.add_argument('--clustering_model',type=str,
                    default="i2r.emerging_topics.incremental_clustering")
args = parser.parse_args()

if os.path.exists(args.tmp_json):
    os.remove(args.tmp_json)
# connect database and init
connection = pymongo.MongoClient(args.ip,args.port)
tdb = connection[args.database_name]

#read documents from DB.Tweets
table = tdb[args.source_table]
table2 = tdb[args.lookup_table]
if args.type == 'tweets_temp':
    topic_id = 4
else:
    topic_id = 2
    
docs = list(table.find({'topic_id':topic_id,'corpus_type':args.type},{'corpus_id':1,'text':1,'timestamp_ms':1, 'text_type':1}))
for doc in docs:
    # if doc['text_type'] != 'text':
    #     continue
    doc_id = doc['corpus_id']
    try:
        user_info = list(table2.find({'_id':doc_id},{'user':1,'retweet_count':1}))[0]
    except:
        print (doc_id)
        continue

    date_string = str(doc['timestamp_ms'])[:-3]
    time_local = time.localtime(int(date_string))
    dt = time.strftime("%Y-%m-%dT%H:%M:%S",time_local)
   
    new_doc = {}
    new_doc['id'] = doc['corpus_id']
    new_doc['content'] = doc['text']
    new_doc['user_id'] = str(user_info['user']['_id'])
    new_doc['followers_count'] = user_info['user']['followers_count']
    new_doc['date'] = dt
    new_doc['retweet_count'] = user_info['retweet_count']
    new_doc['hot_topic_ids'] = []
    with open(args.tmp_json,"a+") as f:
        json.dump(new_doc,f)
        f.write('\n')
# run model
cur = datetime.datetime.now()
date = str(cur.year)+'-'+str(cur.month)+'-'+str(cur.day)+'_'+str(cur.hour)+'_'+str(cur.minute)
cmd = ' python -m '+args.clustering_model + ' '+args.tmp_json + ' --settings ' + args.model_config  +' --save-model ' +'.'+date+'.model  > '+ args.model_output
os.system(cmd)
